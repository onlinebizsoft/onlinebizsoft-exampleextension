<?php
/**
 *MasterPassword Extension
 *
 * PHP versions 4 and 5
 *
 * LICENSE: This source file is subject to version 3.0 of the PHP license
 * that is available through the world-wide-web at the following URI:
 * http://store.onlinebizsoft.com/license.txt.  If you did not receive a copy of
 * the PHP License and are unable to obtain it through the web, please
 * send a note to admin@onlinebizsoft.com so we can mail you a copy immediately.
 *
 * @category   Magento Extensions
 * @package    OnlineBiz_MasterPassword
 * @author     OnlineBiz <sales@onlinebizsoft.com>
 * @copyright  2007-2011 OnlineBiz
 * @license    http://store.onlinebizsoft.com/license.txt
 * @version    1.0.1
 * @link       http://store.onlinebizsoft.com
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();


if (Mage::helper('exampleextension')->isConflict()) {
	
	$feedData = array();
	$feedData[] = array(
		'severity'      => Mage_AdminNotification_Model_Inbox::SEVERITY_MINOR,
		'date_added'    => date('Y-m-d H:i:s'),
		'title'         => 'There are some conflicts between ExampleExtension and existing extensions',
		'description'   => 'Go to System -> Configuration -> STORE.ONLINEBIZSOFT.COM -> Installed Ext. Information for detail',
		'url'           => '',
	);
	
	
} else {
	$feedData = array();
	$feedData[] = array(
		'severity'      => Mage_AdminNotification_Model_Inbox::SEVERITY_NOTICE,
		'date_added'    => date('Y-m-d H:i:s'),
		'title'         => 'ExampleExtension has been installed successfully',
		'description'   => 'Go to System -> Configuration -> STORE.ONLINEBIZSOFT.COM -> ExampleExtension for configuration',
		'url'           => '',
	);
}

Mage::getModel('adminnotification/inbox')->parse(array_reverse($feedData));

$installer->endSetup();

